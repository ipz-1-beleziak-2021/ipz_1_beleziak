
import firebase from "firebase/compat/app";
import "firebase/compat/database";

const firebaseConfig = {
  apiKey: "AIzaSyDrAW0xFMkHB4C7oX1rSu_Yq60fZi3FJBo",
  authDomain: "ipzz-97cdb.firebaseapp.com",
  databaseURL: "https://ipzz-97cdb-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "ipzz-97cdb",
  storageBucket: "ipzz-97cdb.appspot.com",
  messagingSenderId: "731603134856",
  appId: "1:731603134856:web:1d1fc026c1df3927deb23d"
};

firebase.default.initializeApp(firebaseConfig);

export async function GET_DATA(path) {
  let response;
  await firebase.database().ref(path).get().then(data => {
      if(data.exists()) {
        response = data.val();
      } 
      else {
        response = false;
      }
  });
  return response;
}

export async function WRITE_DATA(updates) {
  await firebase.database().ref().update(updates);
}


