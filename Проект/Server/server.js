import http from 'http';
import decodeUriComponent from 'decode-uri-component';
import {login, registration, getAdditionalInformation, putAdditionalInformation, getUserData, searchRoutes, routes, getBookingInfo, booking, getTicketsInfo} from './database.js';


const port = process.env.PORT || 3000;

http.createServer((request, response) => {
    let body = '';
    request.on('data', chunk => {
        body += chunk;
    });
    request.on('end', async () => {
        const func = body.split('=')[0];
        const data = decodeUriComponent(body.split('=')[1]).split('&');
        let resp = '';
        console.log(`Запит: ${func}`);
        if(!!data[0]) {
            console.log(data);
        }
        

        if(func === 'login') {
            resp = await login(data[0], data[1]);
        }
        if(func === 'registration') {
            resp = await registration(data[0], data[1]);
        }
        if(func === 'getAdditionalInformation') {
            resp = await getAdditionalInformation(data[0]);
        }
        if(func === 'putAdditionalInformation') {
            resp = await putAdditionalInformation(data[0], data[1], data[2], data[3], data[4]);
        }
        if(func === 'getUserData') {
            resp = await getUserData(data[0], data[1]);
        }
        if(func === 'searchRoutes') {
            resp = await searchRoutes(data[0]);
        }
        if(func === 'routes') {
            resp = await routes(data[0]);
        }
        if(func === 'getBookingInfo') {
            resp = await getBookingInfo(data[0], data[1]);
        }
        if(func === 'booking') {
            resp = await booking(data[0], data[1], data[2], data[3], data[4], data[5], data[6]);
        }
        if(func === 'getTicketsInfo') {
            resp = await getTicketsInfo(data[0], data[1]);
        }
        if(func === 'logout') {
            resp = 'true';
        }

        if(resp !== 'error') {
            console.log(`Відповідь: ${resp}\n`);
            response.end(resp);
        }
        else {
            console.log(`Database doesn't respond...`);
        }
    });
}).listen(port, () => console.log(`Server started on port ${port}`));