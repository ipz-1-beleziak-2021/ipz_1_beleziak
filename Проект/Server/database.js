import {GET_DATA, WRITE_DATA} from './firebase.js';


export async function login(login, password) {
    let message = '';
    const data = await GET_DATA(`Users/${login}`);
    if(data) {
        if(data.password === password) {
            message = 'true';
        }
        else {
            message = 'Пароль неправильний!';
        }
    } 
    else {
        message = 'Користувач з даним логіном відсутній!';
    }

    return message;
}

export async function registration(login, password) {
    let message = '';
    if(await GET_DATA(`Users/${login}`)) {
        message = 'Користувач з даним логіном вже існує.';
    }
    else {
        let updates = {};
        updates[`Users/${login}/password`] = password;

        await WRITE_DATA(updates);
        message = 'Реєстрація пройшла успішно!';
    }
    return message;
}

export async function getAdditionalInformation(login) {
    let message = '';
    const data = await GET_DATA(`Users/${login}`);
    if(data?.name) {
        message = `${data?.name}\n${data?.surname}\n${data?.email}\n${data?.phone}`
    }
    return message;
}

export async function putAdditionalInformation(login ,name, surname, email, phone) {
    let updates = {};
    updates[`Users/${login}/name`] = name;
    updates[`Users/${login}/surname`] = surname;
    updates[`Users/${login}/email`] = email;
    updates[`Users/${login}/phone`] = phone;
    await WRITE_DATA(updates);

    return 'Інформація успішно добавлена.';
}

export async function routes() {
    let response = '';
    const data = await GET_DATA(`Routes`);
    for(let num in data) {
        response += `${num} ${data[num].route}\n`;
    }

    return response.slice(0,-1);
}

export async function getUserData(login, today) {
    let response = '';
    const data = await GET_DATA(`Users/${login}`);
    response += `${await routes()}&`;
    if(data?.Tickets) {
        Object.values(data.Tickets).forEach((item, index) => {
            if(Number(item.date.split('.')[1]) >= Number(today.split('.')[1]) && Number(item.date.split('.')[0]) >= Number(today.split('.')[0])) {
                response += `${item.date} ${item.route.slice(5, item.length)}                              +${Object.keys(data.Tickets)[index]}\n`;
            }
        });
    }
    return response.slice(0,-1);
}

export async function searchRoutes(text) {
    let response = '';
    const data = (await routes()).split('\n');
    data.forEach(item => {
        if(item.toLowerCase().indexOf(text.toLowerCase()) !== -1) {
            response += `${item}\n`;
        }
    });

    return response.slice(0,-1);
}

export async function getBookingInfo(login, route) {
    let response = await getAdditionalInformation(login) + '&';
    const data = await GET_DATA(`Routes/${route}/Time`);
    for(let price in data) {
        response += `${data[price]}         ${price}грн\n`;
    }

    return response.slice(0,-1);
}

export async function booking(login, route, time, date, name, email, phone) {
    let response = '';
    const time1 = time.split('     ')[0];
    const time2 = time.split('     ')[1];
    let flag = true;
    date = date.split(' ')[0];
    const key = new Date().getTime();

    const data = await GET_DATA(`Users/${login}/Tickets`);
    for(let key in data) {
        if(`${data[key]?.date}${data[key]?.time}` === `${date}${time1}`) {
            flag = false;
            response = '&Ви вже забронювали квиток на дану дату та час.'
            break;
        }
    }
    if(flag) {
        let updates = {};
        updates[`Users/${login}/Tickets/${key}/route`] = route;
        updates[`Users/${login}/Tickets/${key}/time`] = time1;
        updates[`Users/${login}/Tickets/${key}/date`] = date;
        updates[`Users/${login}/Tickets/${key}/name`] = name;
        updates[`Users/${login}/Tickets/${key}/email`] = email;
        updates[`Users/${login}/Tickets/${key}/phone`] = phone;
        updates[`Users/${login}/Tickets/${key}/price`] = time2.trim();
        await WRITE_DATA(updates);
        response = `${key}&Квиток успішно заброньований.`;
    }

    return response;
}

export async function getTicketsInfo(login, key) {
    let response = '';
    const data = await GET_DATA(`Users/${login}/Tickets/${key}`);
    if(data.route) response += `Маршрут: ${data.route}\n`;
    if(data.date) response += `Дата відправлення: ${data.date}\n`;
    if(data.time) response += `Час відправлення: ${data.time}\n`;
    if(data.name) response += `Ім'я та прізвище: ${data.name}\n`;
    if(data.email) response += `Електронна адреса: ${data.email}\n`;
    if(data.phone) response += `Телефон: ${data.phone}\n`;
    if(data.price) response += `Сума: ${data.price}\n`;

    return response;
}