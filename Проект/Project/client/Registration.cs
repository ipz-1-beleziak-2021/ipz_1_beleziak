﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Net;
using System.Collections.Specialized;

namespace client
{
    public partial class Registration : Form
    {
        public Registration()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            Authorization authorization = new Authorization();
            authorization.FormClosed += new FormClosedEventHandler(delegate { Close(); });
            authorization.Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            String login = textBox1.Text;
            String password_1 = textBox2.Text;
            String password_2 = textBox3.Text;
            if (login.Length == 0 || password_1.Length == 0 || password_2.Length == 0)
            {
                MessageBox.Show("Заповніть всі поля!");
            }
            else if (login.Length < 6)
            {
                MessageBox.Show("Логін повинен містити мінімум 6 символів!");
            }
            else if (!Regex.IsMatch(password_1, @"^[0-9a-zA-Z]+$"))
            {
                MessageBox.Show("Логін повинен містити тільки цифри та латинські букви!");
            }
            else if (password_1.Length < 6)
            {
                MessageBox.Show("Пароль повинен містити мінімум 6 символів!");
            }
            else if (!Regex.IsMatch(password_1, @"^[0-9a-zA-Z]+$"))
            {
                MessageBox.Show("Пароль повинен містити тільки цифри та латинські букви!");
            }
            else if (password_1 != password_2)
            {
                MessageBox.Show("Паролі повинні співпадати!");
            }
            else
            {
                try
                {
                    using (var webClient = new MyWebClient())
                    {
                        var pars = new NameValueCollection();
                        pars.Add("registration", login + "&" + password_1);
                        var response = webClient.UploadValues(Variable.URL, pars);
                        string str = System.Text.Encoding.UTF8.GetString(response);
                        if (str == "Користувач з даним логіном вже існує.")
                        {
                            MessageBox.Show(str);
                        }
                        else
                        {
                            MessageBox.Show(str);
                            Authorization authorization = new Authorization();
                            authorization.FormClosed += new FormClosedEventHandler(delegate { Close(); });
                            authorization.Show();
                            this.Hide();
                        }
                    }
                }
                catch (WebException)
                {
                    MessageBox.Show("Сервер не відповідає, спробуйте пізніше.");
                }
            }
        }
    }
}
