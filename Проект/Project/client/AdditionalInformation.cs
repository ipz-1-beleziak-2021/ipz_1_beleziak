﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Collections.Specialized;
using System.Text.RegularExpressions;

namespace client
{
    public partial class AdditionalInformation : Form
    {
        public AdditionalInformation()
        {
            InitializeComponent();
            try
            {
                using (var webClient = new MyWebClient())
                {
                    var pars = new NameValueCollection();
                    pars.Add("getAdditionalInformation", Variable.Login);
                    var response = webClient.UploadValues(Variable.URL, pars);
                    string str = System.Text.Encoding.UTF8.GetString(response);

                    if (str.Length != 0)
                    {
                        var data = str.Split('\n');
                        this.textBox1.Text = data[0];
                        this.textBox2.Text = data[1];
                        this.textBox3.Text = data[2];
                        this.textBox4.Text = data[3];
                    }
                }
            }
            catch (WebException)
            {
                MessageBox.Show("Сервер не відповідає, спробуйте пізніше.");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.textBox1.Text.Length == 0 || this.textBox2.Text.Length == 0 || this.textBox3.Text.Length == 0 || this.textBox4.Text.Length == 0)
            {
                MessageBox.Show("Заповніть всі поля для входу.");
            }
            else if (Regex.IsMatch(this.textBox1.Text, @"[^\u0400-\u04FF]"))
            {
                MessageBox.Show("Поле ім'я повинно містити тільки кирилицю.");
            }
            else if (Regex.IsMatch(this.textBox2.Text, @"[^\u0400-\u04FF]"))
            {
                MessageBox.Show("Поле прізвище повинно містити тільки кирилицю.");
            }
            else if (!Regex.IsMatch(this.textBox3.Text, @"^[^@\s]+@[^@\s]+\.[^@\s]+$"))
            {
                MessageBox.Show("Некоректна електронна адреса.");
            }
            else if (!Regex.IsMatch(this.textBox4.Text, @"^[0-9]+$") || this.textBox4.Text.Length != 10 || this.textBox4.Text[0] != '0')
            {
                MessageBox.Show("Некоректний номер телефону. Приклад: (0xx) xx-xx-xx.");
            }
            else
            {
                try
                {
                    using (var webClient = new MyWebClient())
                    {
                        var pars = new NameValueCollection();
                        pars.Add("putAdditionalInformation", Variable.Login + "&" + this.textBox1.Text + "&" + this.textBox2.Text + "&" + this.textBox3.Text + "&" + this.textBox4.Text);
                        var response = webClient.UploadValues(Variable.URL, pars);
                        string str = System.Text.Encoding.UTF8.GetString(response);
                        MessageBox.Show(str);
                        this.Close();
                    }
                }
                catch (WebException)
                {
                    MessageBox.Show("Сервер не відповідає, спробуйте пізніше.");
                }
            }
            
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
