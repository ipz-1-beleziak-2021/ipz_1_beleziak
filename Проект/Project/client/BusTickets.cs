﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Net;
using System.Collections.Specialized;

namespace client
{
    public partial class BusTickets : Form
    {
        public BusTickets()
        {
            InitializeComponent();
            try
            {
                using (var webClient = new MyWebClient())
                {
                    var pars = new NameValueCollection();
                    pars.Add("getUserData", Variable.Login + "&" + DateTime.Today.ToString().Split(' ')[0]);
                    var response = webClient.UploadValues(Variable.URL, pars);
                    string str = System.Text.Encoding.UTF8.GetString(response);
                    this.listBox1.Items.Clear();
                    this.listBox1.Items.AddRange(str.Split('&')[0].Split('\n'));
                    this.listBox2.Items.Clear();
                    if(str.IndexOf("&") != -1)
                    {
                        this.listBox2.Items.AddRange(str.Split('&')[1].Split('\n'));
                    }
                }
                this.label3.Text = Variable.Login;
            }
            catch (WebException)
            {
                MessageBox.Show("Сервер не відповідає, спробуйте пізніше.");
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            AdditionalInformation additionalInformation = new AdditionalInformation();
            additionalInformation.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(this.textBox1.Text.Length == 0)
            {
                MessageBox.Show("Поле для пошуку не може бути пустим.");
            }
            else if (!Regex.IsMatch(this.textBox1.Text, @"^[0-9]+$"))
            {
                MessageBox.Show("Номер маршруту повинен містити тільки цифри.");
            }
            else
            {
                try
                {
                    using (var webClient = new MyWebClient())
                    {
                        var pars = new NameValueCollection();
                        pars.Add("searchRoutes", this.textBox1.Text);
                        var response = webClient.UploadValues(Variable.URL, pars);
                        string str = System.Text.Encoding.UTF8.GetString(response);
                        if (str.Length == 0)
                        {
                            MessageBox.Show("Пошук не дав результатів.");
                        }
                        else
                        {
                            this.listBox1.Items.Clear();
                            this.listBox1.Items.AddRange(str.Split('\n'));
                        }
                    }
                }
                catch (WebException)
                {
                    MessageBox.Show("Сервер не відповідає, спробуйте пізніше.");
                }
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if(this.textBox1.Text == "")
            {
                try
                {
                    using (var webClient = new MyWebClient())
                    {
                        var pars = new NameValueCollection();
                        pars.Add("routes", "");
                        var response = webClient.UploadValues(Variable.URL, pars);
                        string str = System.Text.Encoding.UTF8.GetString(response);
                        this.listBox1.Items.Clear();
                        this.listBox1.Items.AddRange(str.Split('\n'));
                    }
                }
                catch (WebException)
                {
                    MessageBox.Show("Сервер не відповідає, спробуйте пізніше.");
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (this.textBox1.Text.Length == 0)
            {
                MessageBox.Show("Поле для пошуку не може бути пустим.");
            }
            else if (!Regex.IsMatch(this.textBox1.Text, @"\p{IsCyrillic}"))
            {
                MessageBox.Show("Назва міста повинна містити тільки кирилицю.");
            }
            else
            {
                try
                {
                    using (var webClient = new MyWebClient())
                    {
                        var pars = new NameValueCollection();
                        pars.Add("searchRoutes", this.textBox1.Text);
                        var response = webClient.UploadValues(Variable.URL, pars);
                        string str = System.Text.Encoding.UTF8.GetString(response);
                        if (str.Length == 0)
                        {
                            MessageBox.Show("Пошук не дав результатів.");
                        }
                        else
                        {
                            this.listBox1.Items.Clear();
                            this.listBox1.Items.AddRange(str.Split('\n'));
                        }
                    }
                }
                catch (WebException)
                {
                    MessageBox.Show("Сервер не відповідає, спробуйте пізніше.");
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DialogResult logout = MessageBox.Show("Ви впевнені, що хочете вийти?", "Підтвердження", MessageBoxButtons.YesNo);
            if (logout == DialogResult.Yes)
            {
                try
                {
                    using (var webClient = new MyWebClient())
                    {
                        var pars = new NameValueCollection();
                        pars.Add("logout", this.textBox1.Text);
                        var response = webClient.UploadValues(Variable.URL, pars);
                        string str = System.Text.Encoding.UTF8.GetString(response);
                        if (str == "true")
                        {
                            Authorization authorization = new Authorization();
                            authorization.FormClosed += new FormClosedEventHandler(delegate { Close(); });
                            authorization.Show();
                            this.Hide();
                        }
                        else
                        {
                            MessageBox.Show("Сталася помилка, вийти не вдалось.");
                        }
                    }
                }
                catch (WebException)
                {
                    MessageBox.Show("Сервер не відповідає, спробуйте пізніше.");
                }
            }
        }

        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            Variable.Route = this.listBox1.Text;
            Booking booking = new Booking(this.listBox2);
            booking.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                using (var webClient = new MyWebClient())
                {
                    var pars = new NameValueCollection();
                    pars.Add("getTicketsInfo", Variable.Login + '&' + this.listBox2.Text.Split('+')[1]);
                    var response = webClient.UploadValues(Variable.URL, pars);
                    string str = System.Text.Encoding.UTF8.GetString(response);
                    MessageBox.Show(str);
                }
            }
            catch (WebException)
            {
                MessageBox.Show("Сервер не відповідає, спробуйте пізніше.");
            }
        }
    }
}
