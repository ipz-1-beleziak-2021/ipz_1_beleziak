﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Collections.Specialized;

namespace client
{
    public partial class Authorization : Form
    {
        public Authorization()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String login = textBox1.Text;
            String password = textBox2.Text;

            if (login.Length == 0 || password.Length == 0)
            {
                MessageBox.Show("Заповніть всі поля для входу.");
            }
            else
            {
                try
                {
                    using (var webClient = new MyWebClient())
                    {
                        var pars = new NameValueCollection();
                        pars.Add("login", login + "&" + password);
                        var response = webClient.UploadValues(Variable.URL, pars);
                        string str = System.Text.Encoding.UTF8.GetString(response);

                        if (str == "true")
                        {
                            Variable.Login = this.textBox1.Text;
                            BusTickets busTickets = new BusTickets();
                            busTickets.FormClosed += new FormClosedEventHandler(delegate { Close(); });
                            busTickets.Show();
                            this.Hide();
                        }
                        else
                        {
                            MessageBox.Show(str);
                        }
                    }
                }
                catch (WebException)
                {
                    MessageBox.Show("Сервер не відповідає, спробуйте пізніше.");
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Registration registration = new Registration();
            registration.FormClosed += new FormClosedEventHandler(delegate { Close(); });
            registration.Show();
            this.Hide();
        }
    }
}
