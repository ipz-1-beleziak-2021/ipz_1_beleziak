﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Collections.Specialized;
using System.Text.RegularExpressions;

namespace client
{
    public partial class Booking : Form
    {
        private ListBox list1;
        public Booking(ListBox list)
        {
            list1 = list;
            InitializeComponent();
            this.label1.Text = Variable.Route;
            try
            {
                using (var webClient = new MyWebClient())
                {
                    var pars = new NameValueCollection();
                    pars.Add("getBookingInfo", Variable.Login + '&' + Variable.Route.Split(' ')[0]);
                    var response = webClient.UploadValues(Variable.URL, pars);
                    string str = System.Text.Encoding.UTF8.GetString(response);

                    this.listBox1.Items.AddRange(str.Split('&')[1].Split('\n'));

                    if (str.Split('&')[0].Length != 0)
                    {
                        var data = str.Split('&')[0].Split('\n');
                        this.textBox1.Text = data[0];
                        this.textBox2.Text = data[1];
                        this.textBox3.Text = data[2];
                        this.textBox4.Text = data[3];
                    }
                }
            }
            catch (WebException)
            {
                MessageBox.Show("Сервер не відповідає, спробуйте пізніше.");
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            if(this.dateTimePicker1.Value < DateTime.Today)
            {
                this.dateTimePicker1.Value = DateTime.Today;
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(listBox1.SelectedIndex == -1)
            {
                MessageBox.Show("Оберіть час відправлення.");
            }
            else if (this.textBox1.Text.Length == 0 || this.textBox2.Text.Length == 0 || this.textBox3.Text.Length == 0 || this.textBox4.Text.Length == 0)
            {
                MessageBox.Show("Заповніть всі поля.");
            }
            else if (Regex.IsMatch(this.textBox1.Text, @"[^\u0400-\u04FF]"))
            {
                MessageBox.Show("Поле ім'я повинно містити тільки кирилицю.");
            }
            else if (Regex.IsMatch(this.textBox2.Text, @"[^\u0400-\u04FF]"))
            {
                MessageBox.Show("Поле прізвище повинно містити тільки кирилицю.");
            }
            else if (!Regex.IsMatch(this.textBox3.Text, @"^[^@\s]+@[^@\s]+\.[^@\s]+$"))
            {
                MessageBox.Show("Некоректна електронна адреса.");
            }
            else if (!Regex.IsMatch(this.textBox4.Text, @"^[0-9]+$") || this.textBox4.Text.Length != 10 || this.textBox4.Text[0] != '0')
            {
                MessageBox.Show("Некоректний номер телефону. Приклад: (0xx) xx-xx-xx.");
            }
            else
            {
                try
                {
                    using (var webClient = new MyWebClient())
                    {
                        var pars = new NameValueCollection();
                        pars.Add("booking", Variable.Login + '&' + Variable.Route + '&' + listBox1.Text + '&' + dateTimePicker1.Value + '&' + textBox1.Text + ' ' + textBox2.Text + '&' + textBox3.Text + '&' + textBox4.Text);
                        var response = webClient.UploadValues(Variable.URL, pars);
                        string str = System.Text.Encoding.UTF8.GetString(response);
                        MessageBox.Show(str.Split('&')[1]);
                        if(str != "&Ви вже забронювали квиток на дану дату та час.")
                        {
                            list1.Items.Add(dateTimePicker1.Value.ToString().Split(' ')[0] + ' ' + Variable.Route.Substring(5) + "                              +" + str.Split('&')[0]);
                            this.Close();
                        }
                    }
                }
                catch (WebException)
                {
                    MessageBox.Show("Сервер не відповідає, спробуйте пізніше.");
                }
            }
        }
    }
}
